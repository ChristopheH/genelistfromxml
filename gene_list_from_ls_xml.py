import re # library to split text based on regular expressions
import sys # library to enable arguments from the command line

# function going through a file line by line to extract the targetted gene based on regular expressions
# lines must be in the format xxx__xxx__gene(xxx
# returns a list of distinct genes
def gene_listing(file):
    liste = []
    with open(file, 'r') as listing_xml:
        for ligne in listing_xml:
            challenge = re.split('__', ligne)[2]
            gene = re.split('\(', challenge)[0]
            if gene not in liste:
                liste.append(gene)
    return liste

# reads the file name from the argument following the script name on the command line
file = sys.argv[1]

# calls the gene_listing function onto the file to get a list of distinct genes
list_genes = gene_listing(file)

# prints list_genes
print(list_genes)

# generates the file name that will be used to save the list of genes
# final file name is genes_from_"original file name"_number of distinct genes listed.csv
outfile = "genes_from_{}_{}.csv".format(file,len(list_genes))

# creates and writes the output file
# needs to convert the list into a string with gene names separated by a ","
with open(outfile, 'w') as listing_genes:
    list_genes = str(",".join(list_genes))
    listing_genes.write(list_genes)